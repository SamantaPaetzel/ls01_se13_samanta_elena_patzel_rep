
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* Dies ist
		 * ein Mehrzeiliger
		 * Kommentar
		 */
		
		//print erzeugt Ausgabe ohne Zeilenumbruch
		System.out.println("Heute ist Montag\n");
		//println erzeugt Ausgabe mit Zeilenumbruch
		System.out.print("Morgen ist Dienstag");
		
		
		//Arbeit mit Variablen
		//Deklaration einer Ganzzahligen Variablen "zahl1"
		//Initialisierung mit dem Wert 5
		int zahl1 = 5;
		int zahl2 = 18;
		
		int ergebnis;
		String ausgabe = "F�nf plus Achtzehn ist gleich ";
		ergebnis = zahl1+zahl2;
		System.out.println(ausgabe+ergebnis);
		System.out.println(zahl1);
		System.out.println("Der Wert der Variablen ist: "+zahl1);
		System.out.println(zahl1+zahl2);
		System.out.println(zahl1+"      "+zahl2);
		
		String text = "hello World";
		System.out.print("Hello Class");
		System.out.print(text);}

}
