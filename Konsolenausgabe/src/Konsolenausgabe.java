
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Ich liebe \"Blumen\".");
		System.out.println("Sie duften so gut.\n");
		
		// Der Unterschied zwischen print() zu println() ist, dass bei println() ein Zeilenumbrucg gemacht wird und bei print() wir einfach in einer Zeile weiter geschrieben.
		
		System.out.printf(" %6s\n", "*");
		System.out.printf(" %7s\n", "***");
		System.out.printf(" %8s\n", "*****");
		System.out.printf(" %9s\n", "*******");
		System.out.printf( " %10s\n", "*********");
		System.out.printf( " %3s\n", "***********");
		System.out.println("*************");
		System.out.printf( "  %5s\n", "**");
		System.out.printf( "  %5s\n", "**");
		System.out.printf( "  %5s\n", "**" );
		
		int alter = 24;
		String name = "Sammy";
		System.out.print("\nIch hei�e " + name + " und bin " + alter + " Jahre alt.\n");
		
		double d = 22.4234234;
		System.out.printf("\n%.2f\n" , d);
		d = 111.2222;
		System.out.printf("%.2f\n" , d);
		d = 4.0;
		System.out.printf("%.2f\n" , d);
		d = 1000000.551;
		System.out.printf("%.2f\n" , d);
		d = 97.34;
		System.out.printf("%.2f\n" , d);
		
		System.out.println("   **");
		System.out.println("*      *");
		System.out.println("*      *");
		System.out.println("   **");
		
		System.out.printf("%-5s %s %-19s %s %4s \n", "0!", "=", " ", "=", "1");
		System.out.printf("%-5s %s %-19s %s %4s \n", "1!", "=", "1", "=", "1");
		System.out.printf("%-5s %s %-19s %s %4s \n", "2!", "=", "1 * 2", "=", "2");
		
		
	}

}
